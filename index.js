const express = require('express');
const app = express();
const PORT = process.env.PORT || 56201;

app.use(express.json());
app.use(express.text());
app.post('/square', (req, res) => {
  const number = parseFloat(req.body);
  if (isNaN(number)) {
    return res.status(400).send('Invalid number provided');
  }
  const square = number * number;
  res.json({ number, square });
});

app.post('/reverse', (req, res) => {
  const text = req.body;
  const reversedText = text.split('').reverse().join('');
  res.type('text/plain').send(reversedText);
});

app.get('/date/:year/:month/:day', (req, res) => {
  const { year, month, day } = req.params;
  const inputDate = new Date(`${year}-${month}-${day}`);
  if (isNaN(inputDate.getTime())) {
    return res.status(400).json({ error: 'Invalid date provided' });
  }

  const weekDay = inputDate.toLocaleDateString('en', { weekday: 'long' });
  const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  const currentDate = new Date();
  const difference = Math.abs(Math.ceil((inputDate - currentDate) / (1000 * 3600 * 24)));

  res.json({ weekDay, isLeapYear, difference });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
